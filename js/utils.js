/*
  Misc JavaScript for WineHQ
*/

// load the vars passed on the `src` line
var $_SRC = parseQuery($('script').last().attr('src').replace(/^[^\?]+\??/,''));

// set the web root for all scripts
var web_root = $_SRC.web_root;

/*
 * global document operations
 */
$(document).ready(function()
{
    // top nav - responsive pull menu
    (function(){
        var pull        = $('#whq-tabs .whq-tabs-menu');
            menu        = $('#whq-tabs ul');
            menuHeight  = menu.height();
        $(pull).on('click', function(e) {
            e.preventDefault();
            menu.slideToggle('slow');
        });
        $(window).resize(function(){
            menu.removeAttr('style');
        });
    })();
});

/*
 * global functions
 */

// parse a url query string
function parseQuery (query)
{
    var Params = new Object ();
    if (!query)
        return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for ( var i = 0; i < Pairs.length; i++ )
    {
        var KeyVal = Pairs[i].split('=');
        if ( ! KeyVal || KeyVal.length != 2 )
            continue;
        var key = unescape( KeyVal[0] );
        var val = unescape( KeyVal[1] );
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
   }
   return Params;
}

// done
