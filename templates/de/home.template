<!--VAR:[__layout|open]-->
<!--TITLE:[Windows-Anwendungen unter Linux, BSD, Solaris und macOS ausführen]-->
<!--META_KEYWORDS:[windows, linux, macintosh, solaris, freebsd]-->
<!--META_DESCRIPTION:[Open-Source-Software zum Ausführen von Windows-Anwendungen unter anderen Betriebssystemen.]-->
<!--META_OG:[type|website]-->

<div class="row">
    <div class="col-sm-7">

        <div class="whq-page-container margin-bottom-md">
            <h3 class="title">Aktuelle Versionen</h3>
            <div class="row">
                <div class="col-sm-4 col-md-3">Stabile Version:</div>
                <div class="col-sm-8">
                    <b><a href="https://gitlab.winehq.org/wine/wine/-/releases/wine-{$config_stable_release}">Wine&nbsp;{$config_stable_release}</a></b>
                    <span class="small">
                      (<a href="https://gitlab.winehq.org/wine/wine/-/commits/wine-{$config_stable_release}">Neuerungen (en)</a>)
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">Entwicklungsversion:</div>
                <div class="col-sm-8">
                    <b><a href="https://gitlab.winehq.org/wine/wine/-/releases/wine-{$config_master_release}">Wine&nbsp;{$config_master_release}</a></b>
                    <span class="small">
                        (<a href="https://gitlab.winehq.org/wine/wine/-/commits/wine-{$config_master_release}">Neuerungen (en)</a>)
                    </span>
                </div>
            </div>
        </div>

        <div class="whq-page-container">

            <div class="winehq_menu nopadding margin-bottom-lg">
                <a class="winehq_menu_item info nomargin" href="{$root}/about">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-info" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Über</span>
                    <span class="subtitle">Erfahren Sie mehr über das Wine-Projekt.</span>
                </a>
                <div class="clear"></div>
            </div>

            <p>Wine (ursprünglich ein Akronym für "Wine Is Not an Emulator") ist eine Kompatibilitätsschicht, die es ermöglicht, Windows-Anwendungen unter POSIX-konformen Betriebssystemen auszuführen, wie z.B. Linux, macOS und BSD. Statt interne Windows-Logik zu simulieren, wie eine virtuelle Maschine oder ein Emulator, übersetzt Wine die Windows-API-Aufrufe in Echtzeit zu entsprechenden POSIX-Aufrufen und eliminiert somit die Performance- und Speichereinbußen, die andere Methoden nach sich ziehen. Wine erlaubt es auf diese Weise, Windows-Anwendungen sauber in Ihre Desktopumgebung zu integrieren.</p>

        </div>

    </div>
    <div class="col-sm-5 winehq_menu_wrap fill-height">

        <div class="whq-page-container winehq_menu fill-height">

                <a class="winehq_menu_item dl" href="https://gitlab.winehq.org/wine/wine/-/wikis/Download">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-download" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Download</span>
                    <span class="subtitle">Installieren Sie die neueste Wine-Version.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item help" href="{$root}/help">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-question" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Unterstützung</span>
                    <span class="subtitle">Hilfe zur Verwendung von Wine erhalten.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item devel" href="{$root}/getinvolved">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-users" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Helfen Sie mit</span>
                    <span class="subtitle">Werden Sie ein Teil unserer Gemeinschaft.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item donate" href="{$root}/donate">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-donate" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Spenden</span>
                    <span class="subtitle">An das Wine-Projekt spenden.</span>
                </a>
                <div class="clear"></div>

        </div>

    </div>
</div>

<div class="whq-page-container margin-top-md">

    <h1 class="title"><a href="{$root}/news">Neuigkeiten</a></h1>

    <!--EXEC:[news?n=3]-->

    <p><a href="{$root}/news" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span> weitere Neuigkeiten...</a></p>

</div>

