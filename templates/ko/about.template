<!--TITLE:[Wine 정보]-->

<h1 class="title">Wine 정보</h1>

<p>Wine(원래 "Wine Is Not an Emulator"의 약어)은 리눅스, macOS, BSD와 같은 POSIX
호환 운영체제에서 Windows 프로그램을 실행할 수 있는 호환성 레이어입니다. 가상
머신이나 에뮬레이터와 같이 내부 Windows 로직을 시뮬레이션하는 대신 Wine은
Windows API 호출을 POSIX 시스템 호출로 즉시 대체합니다. 다른 방식과 다르게
성능이나 메모리 문제가 적으며, Windows 프로그램을 데스크톱에 깔끔하게 통합할 수
있습니다.</p>

<p>Wine은 1993년 Bob Amstadt가 Windows 3.1용 프로그램을 리눅스에서 실행시키기
위한 노력에서 시작되었습니다. 프로젝트의 극초반기에 Wine 개발 책임은 Alexandre
Julliard가 맡게 되었으며, 지금도 프로젝트를 관리하고 있습니다. 시간이 지나면서
Windows API와 프로그램이 새로운 하드웨어와 소프트웨어 기능을 활용하기 위해서
진화했고, Wine 역시 새로운 기능을 지원했습니다. 동시에 다른 운영체제로 이식,
안정성 개선, 사용자 경험 개선 등이 이뤄졌습니다.</p>

<p>큰 목표를 가지고 있는 Wine 프로젝트는 2008년 첫 안정 릴리스인 1.0 버전을
출시하기 전까지 약 15년간 개선이 이뤄졌습니다. 몇 차례의 릴리스가 출시된 후에도
Wine은 계속 활발하게 개발되고 있습니다. 여전히 다듬어야 할 곳은 많이 있지만
수많은 사람들이 사용 중인 운영체제에서 Windows 소프트웨어를 구동하기 위해서
Wine을 사용하고 있다고 예상하고 있습니다.</p>

<h3>오픈 소스와 사용자 주도</h3>

<p>Wine은 항상 <a href="https://gitlab.winehq.org/wine/wine/-/wikis/Licensing" title="Wine
라이선스">자유 소프트웨어</a>로 남아 있을 것입니다. 약 절반 정도의 Wine 소스
코드는 자원 봉사자들이 작성했으며, 나머지 부분은 상용 소프트웨어 회사의 지원을
받고 있습니다. Wine의 주 기여자 중에는 Wine의 상용 버전을 판매하는 <a
href="http://www.codeweavers.com/products/" title="CodeWeavers CrossOver
Office">CodeWeavers</a>가 있습니다.</p>

<p>Wine은 사용자 커뮤니티에서도 큰 도움을 받고 있습니다.  자원 봉사자로 구성된
사용자 커뮤니티에서는 <a href="//appdb.winehq.org/" title="Wine 프로그램
데이터베이스">프로그램 데이터베이스</a>에 사용 중인 프로그램의 작동 여부와 팁을
공유하며, 개발자들에게 <a href="//bugs.winehq.org/" title="Bugzilla">버그 추적
시스템</a>을 통해서 문제를 알려 주며, <a href="//forums.winehq.org/"
title="WineHQ 포럼">포럼</a>에 올라온 질문에 답을 합니다.</p>

<h3>더 알아보기:</h3>

<p><i>(다음 중 일부 페이지는 현재 재정비 중이거나 최신 정보가 아닐 수도 있습니다)</i></p>

<ul>
    <li><a href="https://gitlab.winehq.org/wine/wine/-/wikis/Importance-of-Wine">Wine이 중요한 이유</a>
    </li>
    <li><a href="https://gitlab.winehq.org/wine/wine/-/wikis/Wine-History">Wine 프로젝트의 역사</a>
    </li>
    <li><a href="https://gitlab.winehq.org/wine/wine/-/wikis/Project-Organization">프로젝트 조직과 리더십</a>
    </li>
    <li><a href="https://gitlab.winehq.org/wine/wine/-/wikis/Who's-Who">주요 기여자 소개</a>
    </li>
    <li><a href="https://gitlab.winehq.org/wine/wine/-/wikis/Acknowledgements">기타 기여자 목록</a>
    </li>
</ul>
